package sheridan;

public class EmailValidator {

	public static boolean isValidEmail(String email) {

		if (!isValidFormat(email)) {
			return false;
		}
		
		int indexOfAt = email.indexOf("@");
		int indexOfDot = email.indexOf(".");
		
		
		// To separate the email into account, domain, and extentsion,
		// the email's format should be valid.
		String account = email.substring(0, indexOfAt);
		String domain = email.substring(indexOfAt + 1, indexOfDot);
		String extension = email.substring(indexOfDot + 1);
		
		
		boolean isValid1 = isThreeAlphabetsInLowercase(account);
		boolean isValid2 = isDomainThreeAlphabletsOrNumber(domain);
		boolean isValid3 = isNameTwoAlphabets(extension);
		
		/**
		System.out.println("Email: " + email);
		System.out.println("Account: " + account);
		System.out.println("Domain: " + domain);
		System.out.println("Extension: " + extension);
		
		System.out.println("Valid1: " + isValid1);
		System.out.println("Valid2: " + isValid2);
		System.out.println("Valid3: " + isValid3);
		**/
		
		return 	isValid1 && isValid2 && isValid3;
		
	}
	
	
	// Email must be in this format: <account>@<domain>.<extension>
	public static boolean isValidFormat(String email) {
		// Just check if it has @ and .,
		// and the index of at is before .
		
		if (email.contains("@") && email.contains(".")) {
			// Check if it has only one @ symbol
			int countAtSymbol = 0;
			
			for (int i = 0; i < email.length(); i++) {
				if (email.charAt(i) == '@') {
					countAtSymbol++;
					
					// If it has more than 2 at symbol, it will return false
					if (countAtSymbol >= 2) {
						return false;
					}
				}
			}
			
			if (countAtSymbol != 1) {
				return false;
			}
			
			int indexOfAt = email.indexOf("@");
			
			// Check if the . symbol exist after the . mark
			// Check if it has only one . symbol
			int countDotSymbol = 0;
			
			for (int i = indexOfAt; i < email.length(); i++) {
				if (email.charAt(i) == '.') {
					countDotSymbol++;
					
					// If it has more than 2 dot symbol, it will return false
					if (countDotSymbol >= 2) {
						return false;
					}
				}
			}
			
			if (countDotSymbol != 1) {
				return false;
			}
			
			int indexOfDot = email.indexOf(".");
			
			// The dot should come after the at symbol
			if (indexOfDot < indexOfAt) {
				return false;
			}
			
			// The dot should be placed at the very last
			if (indexOfDot == email.length() - 1) {
				return false;
			}
			
			return true;
		}
		
		return false;
	}

	
	// Account name should have at least 3 alpha-characters in lowercase (must not start with a number).
	public static boolean isThreeAlphabetsInLowercase(String account) {
		int countLower = 0;
		
		for (int i = 0; i < account.length(); i++) {
			// Must not start with a number
			if (i == 0 && Character.isDigit(account.charAt(i))) {
				return false;
			}
			
			// Is it lowercase?
			if (Character.isLowerCase(account.charAt(i))) {
				countLower++;
			}
		}
		
		return countLower >= 3;
	}
	
	// Domain name should have at least 3 alpha-characters in lowercase or numbers.
	public static boolean isDomainThreeAlphabletsOrNumber(String domain) {
		int countAlphaLowerOrNumber = 0;
		
		for (int i = 0; i < domain.length(); i++) {
			// Is it a lowercase alphabet or a number?
			if (Character.isDigit(domain.charAt(i)) || Character.isLowerCase(domain.charAt(i))) {
				countAlphaLowerOrNumber++;
			}
		}
		
		return countAlphaLowerOrNumber >= 3;
	}
	
	// Extension name should have at least 2 alpha-characters (no numbers).
	public static boolean isNameTwoAlphabets(String extension) {
		int countLowerAlphabet = 0;
		
		for (int i = 0; i < extension.length(); i++) {
			// No numbers
			if (Character.isDigit(extension.charAt(i))) {
				return false;
			}
			
			// Is it lowercase?
			if (Character.isLowerCase(extension.charAt(i))) {
				countLowerAlphabet++;
			}
		}
		
		return countLowerAlphabet >= 2;
	}
}
