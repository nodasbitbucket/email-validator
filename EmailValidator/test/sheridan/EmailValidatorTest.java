package sheridan;
import static org.junit.Assert.*;
import org.junit.Test;

public class EmailValidatorTest {

	// 16 tests in total
	// 4 tests for Format
	@Test
	public void testIsValidEmailFormatRegular() {
		String email = "nodas@sheridancollege.ca";
		boolean result = EmailValidator.isValidEmail(email);
		assertTrue("Invalid email", result);
	}
	
	@Test
	public void testIsValidEmailFormatException() {
		String email = "";
		boolean result = EmailValidator.isValidEmail(email);
		assertFalse("Invalid email", result);
	}
	
	@Test
	public void testIsValidEmailFormatBoundaryIn() {
		String email = "nod@col.ca";
		boolean result = EmailValidator.isValidEmail(email);
		assertTrue("Invalid email", result);
	}
	
	@Test
	public void testIsValidEmailFormatBoundaryOut() {
		String email = "nod.col@ca";
		boolean result = EmailValidator.isValidEmail(email);
		assertFalse("Invalid email", result);
	}
	
	// 4 tests for @ symbol
	@Test
	public void testIsValidEmailAtSymbolRegular() {
		String email = "nodas@sheridancollege.ca";
		boolean result = EmailValidator.isValidEmail(email);
		assertTrue("Invalid email", result);
	}
	
	@Test
	public void testIsValidEmailAtSymbolException() {
		String email = "@@@";
		boolean result = EmailValidator.isValidEmail(email);
		assertFalse("Invalid email", result);
	}
	
	@Test
	public void testIsValidEmailAtSymbolBoundaryIn() {
		String email = "nodas@college.ca";
		boolean result = EmailValidator.isValidEmail(email);
		assertTrue("Invalid email", result);
	}
	
	@Test
	public void testIsValidEmailAtSymbolBoundaryOut() {
		String email = "no@@col.ca";
		boolean result = EmailValidator.isValidEmail(email);
		assertFalse("Invalid email", result);
	}
	
	
	// 4 tests for the account name that has at least 3 lowercase alphabets and the first character shouldn't be a number
	@Test
	public void testIsValidEmailAccountRegular() {
		String email = "nodas@sheridancollege.ca";
		boolean result = EmailValidator.isValidEmail(email);
		assertTrue("Invalid email", result);
	}
	
	@Test
	public void testIsValidEmailAccountException() {
		String email = "1nodas@sheridancollege.ca";
		boolean result = EmailValidator.isValidEmail(email);
		assertFalse("Invalid email", result);
	}
	
	@Test
	public void testIsValidEmailAccountBoundaryIn() {
		String email = "nod@col.ca";
		boolean result = EmailValidator.isValidEmail(email);
		assertTrue("Invalid email", result);
	}
	
	@Test
	public void testIsValidEmailAccountBoundaryOut() {
		String email = "AbCdE@col.ca";
		boolean result = EmailValidator.isValidEmail(email);
		assertFalse("Invalid email", result);
	}
	
	
	// 4 tests for domain name that has at least 3 lowercase alphabets or numbers
	@Test
	public void testIsValidEmailDomainRegular() {
		String email = "nodas@sheridancollege.ca";
		boolean result = EmailValidator.isValidEmail(email);
		assertTrue("Invalid email", result);
	}
	
	@Test
	public void testIsValidEmailDomainException() {
		String email = "nodas@.ca";
		boolean result = EmailValidator.isValidEmail(email);
		assertFalse("Invalid email", result);
	}
	
	@Test
	public void testIsValidEmailDomainBoundaryIn() {
		String email = "nodas@c12.ca";
		boolean result = EmailValidator.isValidEmail(email);
		assertTrue("Invalid email", result);
	}
	
	@Test
	public void testIsValidEmailDomainBoundaryOut() {
		String email = "AbCdE@col.ca";
		boolean result = EmailValidator.isValidEmail(email);
		assertFalse("Invalid email", result);
	}
	
}
